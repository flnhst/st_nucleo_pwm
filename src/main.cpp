#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <drivers/gpio.h>
#include <drivers/i2c.h>
#include <drivers/pwm.h>

#include <cstdint>
#include <cstdlib>

#include <logging/log.h>

LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);

void set_pwm_usec(const device* pwm_device, std::uint32_t pwm_ch, std::uint32_t period, std::uint32_t pulse)
{
    int ret = pwm_pin_set_usec(pwm_device, pwm_ch, period, pulse, 0);

    if (ret != 0)
    {
        LOG_ERR("Could not set PWM.");
    }
    else
    {
        LOG_INF("PWM channel %i set to period %i, pulse %i.", pwm_ch, period, pulse);
    }
}

void main()
{
    LOG_INF("Booting...");

    auto dev_pwm1 = device_get_binding("PWM_1");
    auto dev_pwm2 = device_get_binding("PWM_2");
    auto dev_pwm3 = device_get_binding("PWM_3");
    auto dev_pwm4 = device_get_binding("PWM_4");

    if (dev_pwm1 == nullptr || dev_pwm2 == nullptr || dev_pwm3 == nullptr || dev_pwm4 == nullptr)
    {
        LOG_ERR("Missing PWM devices.");

        return;
    }

    LOG_INF("Have PWM device.");

    //set_pwm_usec(dev_pwm1, 1, 100, 50); // D7
    //set_pwm_usec(dev_pwm1, 2, 100, 50); // D8

    //set_pwm_usec(dev_pwm2, 1, 100, 50); // A0 D13 -> LED1
    //set_pwm_usec(dev_pwm2, 2, 100, 50); // A1

    //set_pwm_usec(dev_pwm3, 1, 100, 50); // D5

    //set_pwm_usec(dev_pwm4, 3, 100, 50); // D15
    //set_pwm_usec(dev_pwm4, 4, 100, 50); // D14

    //k_msleep(500);

    LOG_INF("Entering main loop.");

    constexpr std::uint32_t periods_count = 8;

    std::uint32_t periods[periods_count] = {100, 200, 300, 400, 500, 600, 700, 800};

    while (true)
    {
        for (int i = 0; i < periods_count; i++)
        {
            set_pwm_usec(dev_pwm4, 3, periods[i], periods[i] / 2);
            //set_pwm_usec(dev_pwm2, 1, periods[i], periods[i] / 2);

            k_msleep(1000);
        }

        k_msleep(1000);
    }
}
