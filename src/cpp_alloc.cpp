#include <logging/log.h>

#include <cstdlib>
#include <cstdint>

LOG_MODULE_REGISTER(cpp_alloc);

void* operator new(std::size_t size)
{
    void* p = std::malloc(size);

    LOG_DBG("p = %p, size = %d", p, size);

    return p;
}

void* operator new[](std::size_t size)
{
    void* p = std::malloc(size);

    LOG_DBG("p = %p, size = %d", p, size);

    return p;
}

void operator delete(void *p)
{
    LOG_DBG("p = %p", p);

    std::free(p);
}

void operator delete[](void *p)
{
    LOG_DBG("p = %p", p);

    std::free(p);
}
